const config = {
  //   extensionsToTreatAsEsm: [".jsx, .js"],
  //   transformIgnorePatterns: ["/node_modules/"],
  //   collectCoverageFrom: ["./src/**/*.{js,jsx}", "!src/**/*.d.js"],
  collectCoverage: true,
  collectCoverageFrom: ["<rootDir>/src/**"],
  setupFiles: ["<rootDir>/src/setupTests.js"],
  testRegex: "/*.test.js$",
  coverageThreshold: {
    global: {
      branches: 10,
      functions: 10,
      lines: 10,
      statements: 10,
    },
    coverageReporters: ["lcov"],
    coverageDirectory: "test-coverage",
  },
  transform: {
    "^.+\\.[t|j]sx?$": "babel-jest",
  },
  moduleDirectories: ["node_modules", "src"],
};

module.exports = config;
