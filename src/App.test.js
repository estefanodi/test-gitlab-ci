import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import App from "./App";

describe("App", () => {
  it("render", () => {
    render(<App />);

    expect(screen.getByTestId("app")).toBeInTheDocument();
  });
});
